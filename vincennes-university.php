<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="assets/gallery/css/unite-gallery.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="project-jumbotron">
      <div class="container">
        <h1>Projects</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row single-project">
        <h2>August 2015:</h2>
          <h4>Vincennes University Parking Lot</h4>
          <p>Remove top soil from designated area, cover entire site with Geotex Fabric for stabilization, haul in and place 550 tons of #53 stone and grade to specifications, install 53 concrete parking bumper on site. </p>
          <div id="gallery" style="display:none;">
    
          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/195.jpg"
              data-image="img/vinu/195.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/196.jpg"
              data-image="img/vinu/196.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/197.jpg"
              data-image="img/vinu/197.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/198.jpg"
              data-image="img/vinu/198.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/199.jpg"
              data-image="img/vinu/199.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/200.jpg"
              data-image="img/vinu/200.jpg"
              data-description="Vincennes University Parking Lot">


          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/201.jpg"
              data-image="img/vinu/201.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/202.jpg"
              data-image="img/vinu/202.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/203.jpg"
              data-image="img/vinu/203.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/204.jpg"
              data-image="img/vinu/204.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/205.jpg"
              data-image="img/vinu/205.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/206.jpg"
              data-image="img/vinu/206.jpg"
              data-description="Vincennes University Parking Lot">


          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/207.jpg"
              data-image="img/vinu/207.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/208.jpg"
              data-image="img/vinu/208.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/209.jpg"
              data-image="img/vinu/209.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/210.jpg"
              data-image="img/vinu/210.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/211.jpg"
              data-image="img/vinu/211.jpg"
              data-description="Vincennes University Parking Lot">

          <img alt="Vincennes University Parking Lot Construction" src="img/vinu/212.jpg"
              data-image="img/vinu/212.jpg"
              data-description="Vincennes University Parking Lot">

    </div> 
    </div>
    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="assets/gallery/js/unitegallery.js"></script>
        <script src="assets/gallery/themes/tiles/ug-theme-tiles.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>

        <script>
          jQuery("#gallery").unitegallery({
            lightbox_type: "compact",
          });
        </script>

    </body>
</html>
