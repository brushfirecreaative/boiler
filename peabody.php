<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="assets/gallery/css/unite-gallery.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="project-jumbotron">
      <div class="container">
        <h1>Projects</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row single-project">
        <h2>2007-14:</h2>
          <h4>Peabody Energy – Miller Creek, Knox Pit – Bicknell, IN</h4>
          <p>Final Reclamation, Built terraces, completed top-soil placement, designed and build flood planes for core streams.</p>
          <hr>

          <h2>2010:</h2>
          <h4>Peabody Energy – Somerville Central Mine – Somerville, IN</h4>
          <p>Completed final reclamation for Somerville Central Mine’s North Pit moving 350,000 ccy of material.</p>
          <hr>

          <h2>2011-12:</h2>
          <h4>Peabody Energy – Somerville Central Mine – Somerville, IN</h4>
          <p>Completed final reclamation for Somerville Central Mine’s West Pit moving 400,000 ccy of material.</p>
          <hr>

          <h2>2012-13:</h2>
          <h4>Peabody Energy – Somerville Central Mine – Somerville, IN</h4>
          <p>Relocated and constructed Smith Fork Creek. </p>
          <hr>

          <h2>2014:</h2>
          <h4>Peabody Energy – Francisco Underground Mine- Francisco, IN</h4>
          <p>Worked with conservancy group constructing levee for slurry impoundment.</p>
          <hr>

          <h2>2014:</h2>
          <h4>Peabody Energy – Somerville Central Mine – Somerville, IN</h4>
          <p>Constructed permanent form of Smith Fork Creek and constructed wetlands, moving 250,000 ccy of material.</p>
          <hr>

          <div id="gallery" style="display:none;">
    
          <img alt="Peabody Energy" src="img/peabody/007.jpg"
              data-image="img/peabody/007.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/008.jpg"
              data-image="img/peabody/008.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/009.jpg"
              data-image="img/peabody/009.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/010.jpg"
              data-image="img/peabody/010.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/011.jpg"
              data-image="img/peabody/011.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/012.jpg"
              data-image="img/peabody/012.jpg"
              data-description="Peabody Energy">


          <img alt="Peabody Energy" src="img/peabody/013.jpg"
              data-image="img/peabody/013.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/014.jpg"
              data-image="img/peabody/014.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/015.jpg"
              data-image="img/peabody/015.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/016.jpg"
              data-image="img/peabody/016.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/017.jpg"
              data-image="img/peabody/017.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/018.jpg"
              data-image="img/peabody/018.jpg"
              data-description="Peabody Energy">


          <img alt="Peabody Energy" src="img/peabody/019.jpg"
              data-image="img/peabody/019.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/020.jpg"
              data-image="img/peabody/020.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/021.jpg"
              data-image="img/peabody/021.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/024.jpg"
              data-image="img/peabody/024.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/027.jpg"
              data-image="img/peabody/027.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/029.jpg"
              data-image="img/peabody/029.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/030.jpg"
              data-image="img/peabody/030.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/031.jpg"
              data-image="img/peabody/031.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/032.jpg"
              data-image="img/peabody/032.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/033.jpg"
              data-image="img/peabody/033.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/034.jpg"
              data-image="img/peabody/034.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/035.jpg"
              data-image="img/peabody/035.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/036.jpg"
              data-image="img/peabody/036.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/037.jpg"
              data-image="img/peabody/037.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/038.jpg"
              data-image="img/peabody/038.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/039.jpg"
              data-image="img/peabody/039.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/040.jpg"
              data-image="img/peabody/040.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/041.jpg"
              data-image="img/peabody/041.jpg"
              data-description="Peabody Energy">

          <img alt="Peabody Energy" src="img/peabody/042.jpg"
              data-image="img/peabody/042.jpg"
              data-description="Peabody Energy">
    </div> 
    </div>
    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="assets/gallery/js/unitegallery.js"></script>
        <script src="assets/gallery/themes/tiles/ug-theme-tiles.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>

        <script>
          jQuery("#gallery").unitegallery({
            lightbox_type: "compact",
          });
        </script>

    </body>
</html>
