<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="industrial-jumbotron">
      <div class="container">
        <h1>Industrial</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row industry-content">
          <h3>Site Work</h3>
          <p>Our company is commonly used as a site work sub contractor for construction projects in the industrial field. This includes all the preliminary measures needed to construct buildings including: demolition, erosion control, earthmoving, grading, compaction, underground utilities, and stone.</p>

          <h3>Earthmoving</h3>
          <p>This process includes stripping top soil and stockpiling for future use, excavating and hauling imported fill material for building pads and structures, and reclaiming excavated site.</p>

          <h3>Grading &amp; Compaction</h3>
          <p>This process includes direct placement of imported material and grading  of that material to plan specified grade. The material is then compacted to specifications using sheep foot and smooth drum compactors.</p>
          <h3>Underground Utilities</h3>
          <p>This process includes installing underground utilities for the construction project such as storm sewers, sanitary sewers, water lines, and gas lines.</p>

          <h3>Erosion Control</h3>
          <p>Erosion control measures include installing silt fence around construction site to prevent erosion, installation of straw blankets on slopes, and importing stone for check damns and construction entrances.</p>

          <h3>Demolition</h3>
          <p>This process uses heavy equipment such as dozers and trackhoes to demolish any building or structures that are required to be removed. Waste material generated from this process is then exported from site using triaxle trucks and hauled to disposal sites.</p>

          <h3>Stone</h3>
          <p>Loudermilk provides stone service which includes importing crushed stone to project site for building pads and backfill of utilities. The stone is then graded and compacted to project specifications.</h3>

      </div>

      <div class="row company-logos industry-logos">
        <h3>Examples of Industrial Work</h3>
        <div class="col-sm-4 col-xs-12">
          <a href="/toyota"><img src="/img/Icons-toyota-boshoku.png" class="toyota logo"></a>
        </div>

        <div class="col-sm-4 col-xs-12">
          <a href="/vincennes-university"><img src="/img/Icons-vincennes-university.png" class="vinu" alt="Vincennes University Logo"></a>
        </div>

        <div class="col-sm-4 col-xs-12">
          <a href="/westport-auto"><img src="/img/Icons-westport.png" class="peabody" alt="Westport Auto Logo"></a>
        </div>
      </div>

    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>
    </body>
</html>
