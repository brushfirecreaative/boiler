<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="assets/gallery/css/unite-gallery.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="project-jumbotron">
      <div class="container">
        <h1>Projects</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row single-project">
        <h2>2015-Present:</h2>
          <h4>Westport Chrysler Dodge Jeep Ram Building Pad Construction</h4>
          <p>Haul in 20,000 ccy of fill material and construct building pad to design grade, install Geotex Fabric on 50% of job site for stabilization, construct detention pond, install sanitary sewer and storm drain system, place rock on building pad to design grade.</p>
          <div id="gallery" style="display:none;">
    
          <img alt="Westport Auto Construction" src="img/westport/140.jpg"
              data-image="img/westport/140.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/142.jpg"
              data-image="img/westport/142.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/143.jpg"
              data-image="img/westport/143.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/144.jpg"
              data-image="img/westport/144.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/147.jpg"
              data-image="img/westport/147.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/150.jpg"
              data-image="img/westport/150.jpg"
              data-description="Westport Auto">


          <img alt="Westport Auto Construction" src="img/westport/152.jpg"
              data-image="img/westport/152.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/154.jpg"
              data-image="img/westport/154.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/164.jpg"
              data-image="img/westport/164.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/174.jpg"
              data-image="img/westport/174.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/175.jpg"
              data-image="img/westport/175.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/184.jpg"
              data-image="img/westport/184.jpg"
              data-description="Westport Auto">


          <img alt="Westport Auto Construction" src="img/westport/185.jpg"
              data-image="img/westport/185.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/215.jpg"
              data-image="img/westport/215.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/217.jpg"
              data-image="img/westport/217.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/220.jpg"
              data-image="img/westport/220.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/224.jpg"
              data-image="img/westport/224.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/226.jpg"
              data-image="img/westport/226.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/227.jpg"
              data-image="img/westport/227.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/228.jpg"
              data-image="img/westport/228.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/230.jpg"
              data-image="img/westport/230.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/233.jpg"
              data-image="img/westport/233.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/234.jpg"
              data-image="img/westport/234.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/235.jpg"
              data-image="img/westport/235.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/227.jpg"
              data-image="img/westport/227.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/228.jpg"
              data-image="img/westport/228.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/230.jpg"
              data-image="img/westport/230.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/233.jpg"
              data-image="img/westport/233.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/234.jpg"
              data-image="img/westport/234.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/235.jpg"
              data-image="img/westport/235.jpg"
              data-description="Westport Auto">


          <img alt="Westport Auto Construction" src="img/westport/236.jpg"
              data-image="img/westport/236.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/237.jpg"
              data-image="img/westport/237.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/239.jpg"
              data-image="img/westport/239.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/240.jpg"
              data-image="img/westport/240.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/267.jpg"
              data-image="img/westport/267.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/268.jpg"
              data-image="img/westport/268.jpg"
              data-description="Westport Auto">


          <img alt="Westport Auto Construction" src="img/westport/269.jpg"
              data-image="img/westport/269.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/270.jpg"
              data-image="img/westport/270.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/279.jpg"
              data-image="img/westport/279.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/280.jpg"
              data-image="img/westport/280.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/281.jpg"
              data-image="img/westport/281.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/282.jpg"
              data-image="img/westport/282.jpg"
              data-description="Westport Auto">


          <img alt="Westport Auto Construction" src="img/westport/283.jpg"
              data-image="img/westport/283.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/284.jpg"
              data-image="img/westport/284.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/285.jpg"
              data-image="img/westport/285.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/286.jpg"
              data-image="img/westport/286.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/287.jpg"
              data-image="img/westport/287.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/288.jpg"
              data-image="img/westport/288.jpg"
              data-description="Westport Auto">


          <img alt="Westport Auto Construction" src="img/westport/290.jpg"
              data-image="img/westport/290.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/291.jpg"
              data-image="img/westport/291.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/292.jpg"
              data-image="img/westport/292.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/293.jpg"
              data-image="img/westport/293.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/294.jpg"
              data-image="img/westport/294.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/301.jpg"
              data-image="img/westport/282.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/303.jpg"
              data-image="img/westport/303.jpg"
              data-description="Westport Auto">

          <img alt="Westport Auto Construction" src="img/westport/304.jpg"
              data-image="img/westport/304.jpg"
              data-description="Westport Auto">

    </div> 
    </div>
    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="assets/gallery/js/unitegallery.js"></script>
        <script src="assets/gallery/themes/tiles/ug-theme-tiles.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>

        <script>
          jQuery("#gallery").unitegallery({
            lightbox_type: "compact",
          });
        </script>

    </body>
</html>
