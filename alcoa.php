<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="assets/gallery/css/unite-gallery.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="project-jumbotron">
      <div class="container">
        <h1>Projects</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row single-project">
        <h2>2014-Present:</h2>
          <h4>A.L.C.O.A. - Squaw Creek Mine - Lynnville, IN</h4>
          <p>Final reclamation of Squaw Creek Mine slurry pit. Drained and filled in final cut moving 3.3 million ccy of material.</p>
          <div id="gallery" style="display:none;">
    
          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/092.jpg"
              data-image="img/alcoa/092.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/093.jpg"
              data-image="img/alcoa/093.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/094.jpg"
              data-image="img/alcoa/094.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/095.jpg"
              data-image="img/alcoa/095.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/096.jpg"
              data-image="img/alcoa/096.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/097.jpg"
              data-image="img/alcoa/097.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">


          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/098.jpg"
              data-image="img/alcoa/098.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/099.jpg"
              data-image="img/alcoa/099.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/100.jpg"
              data-image="img/alcoa/100.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

          <img alt="A.L.C.O.A. - Squaw Creek Mine Reclamation" src="img/alcoa/101.jpg"
              data-image="img/alcoa/101.jpg"
              data-description="A.L.C.O.A. - Squaw Creek Mine">

    </div> 
    </div>
    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="assets/gallery/js/unitegallery.js"></script>
        <script src="assets/gallery/themes/tiles/ug-theme-tiles.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>

        <script>
          jQuery("#gallery").unitegallery({
            lightbox_type: "compact",
          });
        </script>

    </body>
</html>
