<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="assets/gallery/css/unite-gallery.css">
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="project-jumbotron">
      <div class="container">
        <h1>Projects</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row single-project">
        <h2>November 2015:</h2>
          <h4>ATS (Toyota Boshoku) Expansion</h4>
          <p>Site work for expansion on east and west ends of factory totaling in approx. 150,000 sq. ft. of new building. Imported approx. 42,000 cu. yds. of fill material, graded and compacted to design specifications for building pad. Installed storm sewer for new additions and site. </p>
          <div id="gallery" style="display:none;">
    
          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/003.jpg"
              data-image="img/ats/003.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/040.jpg"
              data-image="img/ats/040.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/041.jpg"
              data-image="img/ats/041.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/305.jpg"
              data-image="img/ats/305.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/306.jpg"
              data-image="img/ats/306.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/307.jpg"
              data-image="img/ats/307.jpg"
              data-description="ATS Expansion">


          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/309.jpg"
              data-image="img/ats/309.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/310.jpg"
              data-image="img/ats/310.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/311.jpg"
              data-image="img/ats/311.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/312.jpg"
              data-image="img/ats/312.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/313.jpg"
              data-image="img/ats/313.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/314.jpg"
              data-image="img/ats/314.jpg"
              data-description="ATS Expansion">


          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/316.jpg"
              data-image="img/ats/316.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/317.jpg"
              data-image="img/ats/317.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/318.jpg"
              data-image="img/ats/318.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/319.jpg"
              data-image="img/ats/319.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/320.jpg"
              data-image="img/ats/320.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/321.jpg"
              data-image="img/ats/321.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/322.jpg"
              data-image="img/ats/322.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/323.jpg"
              data-image="img/ats/323.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/325.jpg"
              data-image="img/ats/325.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/326.jpg"
              data-image="img/ats/326.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/327.jpg"
              data-image="img/ats/327.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/343.jpg"
              data-image="img/ats/343.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/344.jpg"
              data-image="img/ats/344.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/345.jpg"
              data-image="img/ats/345.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/346.jpg"
              data-image="img/ats/346.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/347.jpg"
              data-image="img/ats/347.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/348.jpg"
              data-image="img/ats/348.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/349.jpg"
              data-image="img/ats/349.jpg"
              data-description="ATS Expansion">


          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/351.jpg"
              data-image="img/ats/351.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/352.jpg"
              data-image="img/ats/352.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/353.jpg"
              data-image="img/ats/353.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/354.jpg"
              data-image="img/ats/354.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/355.jpg"
              data-image="img/ats/355.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/356.jpg"
              data-image="img/ats/356.jpg"
              data-description="ATS Expansion">


          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/357.jpg"
              data-image="img/ats/357.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/358.jpg"
              data-image="img/ats/358.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/359.jpg"
              data-image="img/ats/359.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/360.jpg"
              data-image="img/ats/360.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/361.jpg"
              data-image="img/ats/361.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/362.jpg"
              data-image="img/ats/362.jpg"
              data-description="ATS Expansion">


          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/364.jpg"
              data-image="img/ats/364.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/365.jpg"
              data-image="img/ats/365.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/366.jpg"
              data-image="img/ats/366.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/367.jpg"
              data-image="img/ats/367.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/368.jpg"
              data-image="img/ats/368.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/369.jpg"
              data-image="img/ats/369.jpg"
              data-description="ATS Expansion">


          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/370.jpg"
              data-image="img/ats/370.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/371.jpg"
              data-image="img/ats/371.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/372.jpg"
              data-image="img/ats/372.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/373.jpg"
              data-image="img/ats/373.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/374.jpg"
              data-image="img/ats/374.jpg"
              data-description="ATS Expansion">

          <img alt="ATS (Toyota Boshoku) Expansion" src="img/ats/375.jpg"
              data-image="img/ats/375.jpg"
              data-description="ATS Expansion">
    </div> 
    </div>
    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="assets/gallery/js/unitegallery.js"></script>
        <script src="assets/gallery/themes/tiles/ug-theme-tiles.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>

        <script>
          jQuery("#gallery").unitegallery({
            lightbox_type: "compact",
            tiles_type:"nested"
          });
        </script>

    </body>
</html>
