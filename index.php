<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="jumbotron">
      <div class="container">
        <a href="tel:+8127264474">Call Today!<br />812.726.4474</a>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row">
        <a href="/peabody"><div class="col-md-4 highlight1">
          <h4>Peabody Somerville Central Mine</h4>
        </div></a>
        <a href="/vinncennes-university"><div class="col-md-4 highlight2">
          <h4>Vincennes University Parking Lot</h4>
       </div></a>
       <a href="/westport-auto"><div class="col-md-4 highlight3">
          <h4>Westport Auto Vincennes, IN</h4>
        </div></a>
      </div>

      <div class="row industries">
        <div class="col-xs-12">
          <ul>
              <a href="/mines"><li class="mines">
                <h6>Mines</h6>
              </li></a>
              <a href="/industrial"><li class="industrial">
                <h6>Industrial</h6>
              </li></a>
              <a href="/earthwork"><li class="earthwork">
                <h6>Earthwork</h6>
              </li></a>
              <a href="/quarry"><li class="quary">
                <h6>Quarry</h6>
              </li></a>
              <a href="/utilities"><li class="utilities">
                <h6>Utilities</h6>
              </li></a>
              <a href="/trucking"><li class="trucking">
                <h6>Trucking</h6>
              </li></a>
              <a href="/maintenance"><li class="maintenance">
                <h6>Maintenance</h6>
              </li></a>
              <a href="/reclamation"><li class="reclamation">
                <h6>Reclamation</h6>
              </li></a>
          </ul>
        </div>
      </div>

      <div class="row company-logos">
        <div class="col-sm-4 col-xs-12">
          <a href="/ats"><img src="/img/Icons-toyota-boshoku.png" class="toyota"></a>
        </div>

        <div class="col-sm-4 col-xs-12">
          <a href="/vincennes-university"><img src="/img/Icons-vincennes-university.png" class="vinu"></a>
        </div>

        <div class="col-sm-4 col-xs-12">
          <a href="/peabody"><img src="/img/Icons-peabody.png" class="peabody"></a>
        </div>
      </div>

    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>
        <script src="js/vendor/dropdown.js"></script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });

          $(".parent-item").click(function(){
            $(".sub-menu").toggle();
          });

        </script>
    </body>
</html>
