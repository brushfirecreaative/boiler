<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>
       <?php
  if (isset($_POST["submit"])) {
    $name = $_POST['name'];
    $email = $_POST['email'];
    $message = $_POST['message'];
    $human = intval($_POST['human']);
    $from = 'Loudermilk Website Contact Form'; 
    $to = 'zach@brushfirecreative.com'; 
    $subject = 'Message from Contact Demo ';
    
    $body = "From: $name\n E-Mail: $email\n Message:\n $message";
 
    // Check if name has been entered
    if (!$_POST['name']) {
      $errName = 'Please enter your name';
    }
    
    // Check if email has been entered and is valid
    if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
      $errEmail = 'Please enter a valid email address';
    }
    
    //Check if message has been entered
    if (!$_POST['message']) {
      $errMessage = 'Please enter your message';
    }
    //Check if simple anti-bot test is correct
    if ($human !== 5) {
      $errHuman = 'Your anti-spam is incorrect';
    }
 
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
  if (mail ($to, $subject, $body, $from)) {
    $result='<div class="alert alert-success">Thank You! We will be in touch soon.</div>';
  } else {
    $result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
  }
}
  }
?>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="contact-jumbotron">
      <div class="container">
        <h1>Contact</h1>
      </div>
    </div>



    <div class="container" style="margin-top: 30px;">
      <!-- Example row of columns -->
      <div class="row contact-content">
        <div class="col-md-6">
            <a href="https://www.google.com/maps/dir//2048+N.+LOUDERMILK+RD,+Vincennes,+IN/" target="_blank"><img src="http://maps.googleapis.com/maps/api/staticmap?center=2048+N.+LOUDERMILK+RD,+Vincennes,+IN&zoom=13&scale=2&size=640x300&maptype=roadmap&format=png&visual_refresh=true&markers=size:mid%7Ccolor:0x4593fc%7Clabel:%7C2048+N.+LOUDERMILK+RD,+Vincennes,+IN" alt="Google Map of 2048 N. LOUDERMILK RD, Vincennes, IN" style="max-width: 100%; height: auto; margin-bottom: 25px;"></a>
        </div>
      <div class="col-md-6">
        <form class="form-horizontal" role="form" method="post" action="contact.php">
  <div class="form-group">
    <!-- <label for="name" class="col-sm-2 control-label">Name</label> -->
    <div class="col-sm-10">
      <input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="">
      <?php echo "<p class='text-danger'>$errName</p>";?>
    </div>
  </div>
  <div class="form-group">
    <!-- <label for="email" class="col-sm-2 control-label">Email</label> -->
    <div class="col-sm-10">
      <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="">
      <?php echo "<p class='text-danger'>$errEmail</p>";?>
    </div>
  </div>
  <div class="form-group">
    <!-- <label for="message" class="col-sm-2 control-label">Message</label> -->
    <div class="col-sm-10">
      <textarea class="form-control" rows="4" name="message" placeholder="Your Message"></textarea>
      <?php echo "<p class='text-danger'>$errMessage</p>";?>
    </div>
  </div>
  <div class="form-group">
    <!-- <label for="human" class="col-sm-2 control-label">2 + 3 = ?</label> -->
    <div class="col-sm-10">
      <input type="text" class="form-control" id="human" name="human" placeholder="2 + 3 = ?">
      <?php echo "<p class='text-danger'>$errHuman</p>";?>
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-10">
      <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-10 col-sm-offset-1">
      <?php echo $result; ?>
    </div>
  </div>
</form>
      </div>
    </div>

    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>
    </body>
</html>
