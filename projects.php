<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="project-jumbotron">
      <div class="container">
        <h1>Projects</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row projectrow">

        <a href="/alcoa"><div class="col-md-4 project-boxes dark-box box1">
          <div class="box-info">
            <h2>Present</h2>
            <hr>
            <h4>A.L.C.O.A.<br />Squaw Creek Mine<br />Lynnville, IN<h4>
          </div>
        </div></a>

         <a href="/westport"><div class="col-md-4 project-boxes light-box box2">
          <div class="box-info">
            <h2>Present</h2>
            <hr>
            <h4>Westport Chrysler<br />Dodge Jeep Ram<br />Building Pad<br />Construction</h4>
          </div>
        </div></a>

         <a href="/ats"><div class="col-md-4 project-boxes gray-box box3">
          <div class="box-info">
            <h2>2015-Nov</h2>
            <hr>
            <h4>ATS<br />(Toyota Boshoku)<br />Expansion<h4>
          </div>
        </div></a>
      </div>

       <div class="row projectrow">
        <a href="/vincennes-university"><div class="col-md-4 project-boxes gray-box box1">
          <div class="box-info">
            <h2>2015 - Aug</h2>
            <hr>
            <h4>Vincennes<br />University<br />Parking Lot<h4>
          </div>
        </div></a>

         <a href="/peabody"><div class="col-md-4 project-boxes dark-box box2">
          <div class="box-info">
            <h2>2014</h2>
            <hr>
            <h4>Peadbody Energy<br />Somerville<br />Central Mine<br />Somerville, IN</h4>
          </div>
        </div></a>

        <a href="/peabody"><div class="col-md-4 project-boxes light-box box3">
          <div class="box-info">
            <h2>2014</h2>
            <hr>
            <h4>Peadbody Energy<br />Francisco<br />Underground Mine<br />Francsico, IN<h4>
          </div>
        </div></a>
      </div>

      <div class="row projectrow"> 
        <a href="/sun-energy"><div class="col-md-4 project-boxes light-box box1">
          <div class="box-info">
            <h2>2014</h2>
            <hr>
            <h4>Sun Energy<br />Hilsmeyer #1<br />&amp; #2 Mines<br />Stendal, IN<h4>
          </div>
        </div></a>

        <a href="/peabody"><div class="col-md-4 project-boxes gray-box box2">
          <div class="box-info">
            <h2>2012-13</h2>
            <hr>
            <h4>Peadbody Energy<br />Somerville<br />Central Mine<br />Somerville, IN</h4>
          </div>
        </div></a>

        <a href="/peabody"><div class="col-md-4 project-boxes dark-box box3">
          <div class="box-info">
            <h2>2011-12</h2>
            <hr>
            <h4>Peadbody Energy<br />Somerville<br />Central Mine<br />Somerville, IN<h4>
          </div>
        </div></a>
      </div>

      <div class="row projectrow1">
        <a href="/peabody"><div class="col-md-4 project-boxes dark-box box1">
          <div class="box-info">
            <h2>2010</h2>
            <hr>
            <h4>Peadbody Energy<br />Somerville<br />Central Mine<br />Somerville, IN<h4>
          </div>
        </div></a>

        <a href="/peabody"><div class="col-md-4 project-boxes light-box box2">
          <div class="box-info">
            <h2>2012-13</h2>
            <hr>
            <h4>Peadbody Energy<br />Miller Creek<br />Knox Pit<br />Bicknell, IN</h4>
          </div>
        </div></a>

        <div class="col-md-4 project-boxes gray-box box3">

        </div>
      </div>

    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>
    </body>
</html>
