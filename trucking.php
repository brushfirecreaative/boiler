<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="favicon.png"> 
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:400,300,700' rel='stylesheet' type='text/css'>
        <style>
            body {

            }
        </style>
        <link rel="stylesheet" href="css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
      <header>This will be replaced with the contents of header.html</header>

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <section class="page-content">
    <div class="trucking-jumbotron">
      <div class="container">
        <h1>Trucking</h1>
      </div>
    </div>

    <div class="container">
      <!-- Example row of columns -->
      <div class="row industry-content">
          <h3>Heavy Equipment Hauling</h3>
          <p>Loudermilk offers the service of hauling heavy equipment that is oversize and overweight. Heavy equipment is hauled using a semi and detach trailer. </p>

          <h3>Triaxle</h3>
          <p>Loudermilk uses triaxle trucks to haul material for customers over the road. Materials commonly hauled include stone, dirt, and waste material.</p>

          <h3>Agriculture</h3>
          <p>Loudermilk Contracting uses semi's and drop deck trailers to haul agriculture materials and equipment. Tractors, implements, and straw are commonly hauled in this process.</p>

      </div>

      <div class="row company-logos industry-logos">
        <h3>Examples of Trucking Work</h3>
        <div class="col-sm-4 col-xs-12">
          <a href="/toyota"><img src="/img/Icons-toyota-boshoku.png" class="toyota logo"></a>
        </div>

        <div class="col-sm-4 col-xs-12">
          <a href="/vincennes-university"><img src="/img/Icons-vincennes-university.png" class="vinu" alt="Vincennes University Logo"></a>
        </div>

        <div class="col-sm-4 col-xs-12">
          <a href="/westport-auto"><img src="/img/Icons-westport.png" class="peabody" alt="Westport Auto Logo"></a>
        </div>
      </div>

    </section>

      <footer>This will be replaced with the contents of footer.html</footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
        <!-- code for including header/footer -->
        <script type="text/javascript">
         
          // we're ready, include away
          $(window).ready(function(){
           
            // use easy AJAX jQuery helpers
           
            // add header.html into <header>
            $.get("header.html", function(fileData){
              $('header').html(fileData);
            });
           
            // add footer.html into <footer>
            $.get("footer.html", function(fileData){
              $('footer').html(fileData);
            });
           
          });
        </script>
    </body>
</html>
